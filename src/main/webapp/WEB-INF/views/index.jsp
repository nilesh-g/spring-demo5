<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Login</title>
</head>
<body>
	<h3><s:message code="app.title"/></h3>
	<form method="post" action="auth">
		<s:message code="email.label"/>: <input type="email" name="email"/> <br/>
		<s:message code="password.label"/>: <input type="password" name="password"/> <br/>
		<input type="submit" value="Sign In"/>
		<!-- <a href="newuser">Sign Up</a>  -->
		<a href="register">Sign Up</a>
	</form>
	
	<a href="login?locale=hi">Hindi</a>
	<a href="login?locale=mr">Marathi</a>
	<a href="login?locale=en">English</a>
</body>
</html>
