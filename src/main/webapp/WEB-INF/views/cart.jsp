<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Cart</title>
<link rel="stylesheet" href="css/site.css"/>
</head>
<body>
	<table>
		<thead>
			<tr>
				<td>Id</td>
				<td>Name</td>
				<td>Author</td>
				<td>Subject</td>
				<td>Price</td>
			</tr>
		</thead>
		<tbody>
			<c:forEach var="b" items="${cartList}">
				<tr>
					<td>${b.id}</td>
					<td>${b.name}</td>
					<td>${b.author}</td>
					<td>${b.subject}</td>
					<td>${b.price}</td>
				</tr>			
			</c:forEach>		
		</tbody>
	</table>
	
	<a href="logout">Sign Out</a>
</body>
</html>

