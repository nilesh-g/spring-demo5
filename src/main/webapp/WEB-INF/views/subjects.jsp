<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Subjects</title>
</head>
<body>
	<sf:form method="post" modelAttribute="command" action="books">
		<sf:radiobuttons path="subject" items="${subjectList}" delimiter="<br/>"/>
		<br/><input type="submit" value="Show Books"/>
		<a href="showcart">Show Cart</a>
	</sf:form>
</body>
</html>

