<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Registration</title>
</head>
<body>
	<sf:form method="post" modelAttribute="cust" action="register">
		Name: <sf:input path="name"/> <sf:errors path="name"/> <br/>
		Email: <sf:input path="email"/> <sf:errors path="email"/> <br/>
		Mobile: <sf:input path="mobile"/> <sf:errors path="mobile"/> <br/>
		Password: <sf:password path="password"/> <sf:errors path="password"/> <br/>
		Address: <sf:input path="address"/> <sf:errors path="address"/> <br/>
		<input type="submit" value="Register"/>
		<a href="login">Sign In</a>
		<br/><br/>
		${message}
		<br/>
		<sf:errors path="*"/>
	</sf:form>
</body>
</html>