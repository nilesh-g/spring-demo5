<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Books</title>
</head>
<body>
	<sf:form action="addcart">
		<sf:checkboxes path="book" items="${bookList}" delimiter="<br/>" itemLabel="name" itemValue="id"/>
		<br/><input type="submit" value="Add Cart"/>
	</sf:form>
</body>
</html>