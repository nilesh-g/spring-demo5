package com.sunbeaminfo.sh.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeaminfo.sh.daos.BookDao;
import com.sunbeaminfo.sh.entities.Book;

//@Transactional
@Service
public class BookServiceImpl implements BookService {
	@Autowired
	private BookDao bookDao;
	
	@Transactional
	public Book getBook(int id) {
		return bookDao.getBook(id);
	}
	
	@Transactional
	public void addBook(Book b) {
		bookDao.addBook(b);
	}
	
	@Transactional
	public void updateBook(Book b) {
		bookDao.updateBook(b);
	}
	
	@Transactional
	public void deleteBook(int id) {
		bookDao.deleteBook(id);
	}
	
	@Transactional
	public List<Book> getBooks() {
		return bookDao.getBooks();
	}
	
	@Transactional
	public List<Book> getSubjectBooks(String subject) {
		return bookDao.getSubjectBooks(subject);
	}
	
	@Transactional
	public List<String> getSubjects() {
		return bookDao.getSubjects();
	}
}


