package com.sunbeaminfo.sh.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sunbeaminfo.sh.daos.CustomerDao;
import com.sunbeaminfo.sh.entities.Customer;
import com.sunbeaminfo.sh.models.Login;

@Transactional
@Service
public class CustomerServiceImpl implements CustomerService {
	@Autowired
	private CustomerDao customerDao;
	
	@Override
	public Customer getCustomer(String email) {
		return customerDao.getCustomer(email);
	}

	@Override
	public void addCustomer(Customer cust) {
		customerDao.addCustomer(cust);
	}

	@Override
	public int changePassword(Customer cust, String newPassword) {
		return customerDao.changePassword(cust, newPassword);
	}
	
	@Override
	public Customer authenticate(Login login) {
		Customer c = getCustomer(login.getEmail());
		if(c != null && c.getPassword().equals(login.getPassword()))
			return c;
		return null;
	}

}
