package com.sunbeaminfo.sh.services;

import com.sunbeaminfo.sh.entities.Customer;
import com.sunbeaminfo.sh.models.Login;

public interface CustomerService {

	Customer getCustomer(String email);

	void addCustomer(Customer cust);

	int changePassword(Customer cust, String newPassword);

	Customer authenticate(Login login);
}