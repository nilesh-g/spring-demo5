package com.sunbeaminfo.sh.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sunbeaminfo.sh.entities.Book;
import com.sunbeaminfo.sh.models.BookIds;
import com.sunbeaminfo.sh.models.ShoppingCart;
import com.sunbeaminfo.sh.models.Subject;
import com.sunbeaminfo.sh.services.BookService;

@Controller
public class ShopControllerImpl {
	@Autowired
	private BookService bookService;
	@Autowired
	private ShoppingCart shopCart;
	
	@RequestMapping("/subjects") // reqUrl
	public String subjects(Map<String,Object> model) {
		List<String> list = bookService.getSubjects();
		model.put("subjectList", list);
		Subject selSubject = new Subject();
		if(list.size() > 0)
			selSubject.setSubject(list.get(0));
		model.put("command", selSubject);
		return "subjects"; // viewName
	}
	
	@RequestMapping("/books") // reqUrl
	public String subjectBooks(Subject selSubject, Map<String,Object> model) {
		String subject = selSubject.getSubject();
		List<Book> list = bookService.getSubjectBooks(subject);
		model.put("bookList", list);
		BookIds selBooks = new BookIds();
		model.put("command", selBooks);
		return "books"; // viewName
	}
	
	@RequestMapping("/addcart") // reqUrl
	public String addcart(BookIds bookIds, Map<String,Object> model, HttpSession session) {
		//List<Integer> cart = (List<Integer>) session.getAttribute("cart"); // not needed if used session scoped ShoppingCart bean
		List<Integer> cart = shopCart.getBookIds();
		for (String bookId : bookIds.getBook()) {
			int id = Integer.parseInt(bookId);
			cart.add(id);
		}
		return "forward:subjects";
	}
	
	@RequestMapping("/showcart")
	public String showcart(Map<String,Object> model, HttpSession session) {
		//List<Integer> cart = (List<Integer>) session.getAttribute("cart"); // not needed if used session scoped ShoppingCart bean
		List<Integer> cart = shopCart.getBookIds();
		List<Book> cartList = new ArrayList<Book>();
		for (int id : cart) {
			Book b = bookService.getBook(id);
			cartList.add(b);
		}
		model.put("cartList", cartList);
		return "cart";
	}
	
	@RequestMapping("/logout")
	public String logout(HttpSession session) {
		session.invalidate();
		return "logout";
	}
	
	@RequestMapping("/allbooks")
	public @ResponseBody List<Book> getAllBooks() {
		return bookService.getBooks();
	}
}


