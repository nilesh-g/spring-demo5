package com.sunbeaminfo.sh.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sunbeaminfo.sh.entities.Book;
import com.sunbeaminfo.sh.services.BookService;

@RestController
public class BookRestController {
	@Autowired
	private BookService bookService;

	@GetMapping("/book")
	public List<Book> getAllBooks() {
		return bookService.getBooks();
	}
	
	@GetMapping("/bookresp/{id}")
	public ResponseEntity<Book> getBookResp(@PathVariable("id") int id, HttpServletResponse response) {
		ResponseEntity<Book> resp = null;

		try {
			Book b = bookService.getBook(id);
			if (b != null)
				resp = new ResponseEntity<Book>(b, HttpStatus.OK);
			else {
				//response.sendError(422);
				//response.addHeader("allow-cross-origin-request", "*");
				resp = new ResponseEntity<Book>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			resp = new ResponseEntity<Book>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return resp;
	}
	
	@GetMapping(value="/bookcust/{id}", produces = "application/json")
	public Map<String, Object> getBookCust(@PathVariable("id") int id) {
		Book b = bookService.getBook(id);
		Map<String, Object> resp = new HashMap<String, Object>();
		resp.put("id", b.getId());
		resp.put("title", b.getName());
		resp.put("price", b.getPrice());
		return resp;
	}
	
	@GetMapping("/book/{id}")
	public Book getBook(@PathVariable("id") int id) {
		return bookService.getBook(id);
	}
	
	@PostMapping("/book")
	public boolean newBook(@RequestBody Book b) {
		try {
			bookService.addBook(b);
			return true;
		} catch (Exception e) {
			return false;
		}
	}
	
	@DeleteMapping("/book/{id}")
	public boolean deleteBook(@PathVariable("id") int id) {
		try {
			bookService.deleteBook(id);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@PutMapping("/book/{id}")
	public boolean updateBook(@PathVariable("id") int id, @RequestBody Book b) {
		try {
			bookService.updateBook(b);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}	
