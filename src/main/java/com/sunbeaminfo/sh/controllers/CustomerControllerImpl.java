package com.sunbeaminfo.sh.controllers;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.sunbeaminfo.sh.entities.Customer;
import com.sunbeaminfo.sh.models.Login;
import com.sunbeaminfo.sh.services.CustomerService;

@Controller
public class CustomerControllerImpl {
	@Autowired
	private CustomerService customerService;

	@RequestMapping("/login")
	public String login(Locale locale) {
		System.out.println("Current locale: " + locale);
		return "index";
	} 

	@RequestMapping("/auth")
	public String authCustomer(Map<String,Object> model, Login login, HttpSession session) {
		Customer cust = customerService.authenticate(login);
		if(cust == null) {
			model.put("message", "Invalid username or password.");
			return "failed"; // failed
		} else {
			//model.put("cust", cust);
			//return "welcome"; // success
			
			// create empty cart ArrayList<Integer> and add to session
			//session.setAttribute("cart", new ArrayList<Integer>()); // not needed if used session scoped ShoppingCart bean
			return "redirect:subjects"; // success
		}
	}
	
	/*
	@RequestMapping("/newuser")
	public String newUser() {
		return "registration";
	}
	
	@RequestMapping("/register")
	public String register(Customer cust, Map<String, Object> model) {
		try {
			customerService.addCustomer(cust);
			model.put("message", "Customer registered successfully.");
		} catch (Exception e) {
			e.printStackTrace();
			model.put("message", "Customer registration failed.");
		}
		return "registration";
	}
	*/
	
	//@RequestMapping(value="/register", method = RequestMethod.GET)
	@GetMapping("/register")
	public String newUser(Map<String, Object> model) {
		model.put("cust", new Customer());
		return "registration";
	}
	
	//@RequestMapping(value="/register", method = RequestMethod.POST)
	@PostMapping("/register")
	public String register(@Valid @ModelAttribute("cust") Customer cust, BindingResult res,  Map<String, Object> model) {
		//model.put("cust", cust);
		try {
			if(res.hasErrors()) {
				model.put("message", "Customer data is not valid.");
			} else {
				customerService.addCustomer(cust);
				model.put("message", "Customer registered successfully.");
			}
		} catch (Exception e) {
			e.printStackTrace();
			model.put("message", "Customer registration failed.");
		}
		return "registration";
	}
}
