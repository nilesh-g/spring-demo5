package com.sunbeaminfo.sh.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "CUSTOMERS")
public class Customer implements Serializable {
	@Id
	@GeneratedValue(generator = "g", strategy = GenerationType.IDENTITY)
	@Column
	private int id;
	@NotEmpty(message = "*")
	@Column
	private String name;
	@NotEmpty(message = "*")
	@Column
	private String address;
	@NotEmpty(message = "*")
	@Pattern(regexp="^[0-9]{10}$")
	@Column
	private String mobile;
	@NotEmpty(message = "*")
	@Email
	@Column
	private String email;
	@NotEmpty(message = "*")
	@Length(min = 4)
	@Column
	private String password;
	
	public Customer() {
		this(0, "", "", "", "", "");
	}

	public Customer(int id, String name, String address, String mobile, String email, String password) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.mobile = mobile;
		this.email = email;
		this.password = password;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	@Override
	public String toString() {
		return String.format("Customer [id=%s, name=%s, address=%s, mobile=%s, email=%s, password=%s]", id, name,
				address, mobile, email, password);
	}
}





