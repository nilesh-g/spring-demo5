package com.sunbeaminfo.sh.daos;

import java.util.List;

import com.sunbeaminfo.sh.entities.Book;

public interface BookDao {

	Book getBook(int id);

	void addBook(Book b);

	void updateBook(Book b);

	void deleteBook(int id);

	List<Book> getBooks();

	List<Book> getSubjectBooks(String subject);

	List<String> getSubjects();

}