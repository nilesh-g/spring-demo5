package com.sunbeaminfo.sh.daos;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sunbeaminfo.sh.entities.Customer;

@Repository
public class CustomerDaoImpl implements CustomerDao {
	@Autowired
	private SessionFactory factory;
	@Override
	public Customer getCustomer(String email) {
		Session session = factory.getCurrentSession();
		Query<Customer> q = session.createQuery("from Customer c where c.email=:p_email");
		q.setParameter("p_email", email);
		return q.getSingleResult();
	}
	
	@Override
	public void addCustomer(Customer cust) {
		Session session = factory.getCurrentSession();
		session.persist(cust);
	}
	
	@Override
	public int changePassword(Customer cust, String newPassword) {
		Session session = factory.getCurrentSession();
		String hql = "update Customer c set c.password=:p_newPassword where c.id=:p_id";
		Query q = session.createQuery(hql);
		q.setParameter("p_newPassword", newPassword);
		q.setParameter("p_id", cust.getId());
		int cnt = q.executeUpdate();
		session.refresh(cust);
		return cnt;
	}
}
