package com.sunbeaminfo.sh.daos;

import com.sunbeaminfo.sh.entities.Customer;

public interface CustomerDao {

	Customer getCustomer(String email);

	void addCustomer(Customer cust);

	int changePassword(Customer cust, String newPassword);

}