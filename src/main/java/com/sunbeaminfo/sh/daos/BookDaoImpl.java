package com.sunbeaminfo.sh.daos;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.ParameterMode;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.jdbc.ReturningWork;
import org.hibernate.jdbc.Work;
import org.hibernate.procedure.ProcedureCall;
import org.hibernate.query.NativeQuery;
import org.hibernate.query.Query;
import org.hibernate.transform.ResultTransformer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sunbeaminfo.sh.entities.Book;

@Repository
public class BookDaoImpl implements BookDao {
	@Autowired
	private SessionFactory factory;
	
	@Override
	public Book getBook(int id) {
		Session session = factory.getCurrentSession();
		return session.get(Book.class, id);
	}
	@Override
	public void addBook(Book b) {
		Session session = factory.getCurrentSession();
		session.persist(b);
	}
	@Override
	public void updateBook(Book b) {
		Session session = factory.getCurrentSession();
		session.update(b);
	}
	@Override
	public void deleteBook(int id) {
		Session session = factory.getCurrentSession();
		Book b = getBook(id);
		if(b != null)
			session.remove(b);
	}
	@Override
	public List<Book> getBooks() {
		Session session = factory.getCurrentSession();
		String hql = "from Book b";
		Query q = session.createQuery(hql);
		return q.getResultList();
	}
	@Override
	public List<Book> getSubjectBooks(String subject) {
		Session session = factory.getCurrentSession();
		Query q = session.getNamedQuery("hqlSubjectBooks");
		q.setParameter(1, subject);
		return q.getResultList();
	}
	@Override
	public List<String> getSubjects() {
		Session session = factory.getCurrentSession();
		Query q = session.getNamedQuery("hqlSubjects");
		return q.getResultList();
	}
}







