package com.sunbeaminfo.sh.models;

import java.util.Arrays;

public class BookIds {
	private String[] book;
	public BookIds() {
		this.book = new String[0];
	}
	public BookIds(String[] book) {
		this.book = book;
	}
	public String[] getBook() {
		return book;
	}
	public void setBook(String[] book) {
		this.book = book;
	}
	@Override
	public String toString() {
		return String.format("BookIds [book=%s]", Arrays.toString(book));
	}
}
