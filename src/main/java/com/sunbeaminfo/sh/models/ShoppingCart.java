package com.sunbeaminfo.sh.models;

import java.util.List;

public interface ShoppingCart {

	List<Integer> getBookIds();

	void setBookIds(List<Integer> bookIds);

}