package com.sunbeaminfo.sh.models;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Scope(value="session", proxyMode = ScopedProxyMode.INTERFACES)
@Component
public class ShoppingCartImpl implements ShoppingCart {
	private List<Integer> bookIds;
	public ShoppingCartImpl() {
		this.bookIds = new ArrayList<Integer>();
	}
	@Override
	public List<Integer> getBookIds() {
		return bookIds;
	}
	@Override
	public void setBookIds(List<Integer> bookIds) {
		this.bookIds = bookIds;
	}
	@Override
	public String toString() {
		return String.format("ShoppingCart [bookIds=%s]", bookIds);
	}
}
